package com.ruyuan.little.project.elasticsearch.biz.common.dto;

import lombok.Data;
@Data
public class CommonResponse<T> {
    private int code;
    private String message;
    private T data;

    public CommonResponse(int code, String message, T data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public static <T> CommonResponse<T> success(T data) {
        return new CommonResponse<>(200, "请求成功", data);
    }
    public static <T> CommonResponse<T> success() {
        return new CommonResponse<>(200, "请求成功",null);
    }
    public static <T> CommonResponse<T> fail() {
        return new CommonResponse<>(500, "失败", null);
    }

    // Getter and Setter methods here...

    // ...
}
