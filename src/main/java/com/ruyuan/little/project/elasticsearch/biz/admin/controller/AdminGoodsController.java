package com.ruyuan.little.project.elasticsearch.biz.admin.controller;

import com.alibaba.fastjson.JSON;

import com.ruyuan.little.project.elasticsearch.biz.admin.dto.AdminGoodsSkuAddDTO;
import com.ruyuan.little.project.elasticsearch.biz.admin.dto.AdminGoodsSpuAddDTO;
import com.ruyuan.little.project.elasticsearch.biz.admin.dto.AdminGoodsSpuDTO;
import com.ruyuan.little.project.elasticsearch.biz.admin.dto.AdminReindexDTO;
import com.ruyuan.little.project.elasticsearch.biz.admin.entity.AdminGoodsSku;
import com.ruyuan.little.project.elasticsearch.biz.admin.entity.AdminGoodsSpu;
import com.ruyuan.little.project.elasticsearch.biz.admin.service.AdminGoodsService;
import com.ruyuan.little.project.elasticsearch.biz.common.constant.StringPoolConstant;
import com.ruyuan.little.project.elasticsearch.biz.common.dto.CommonResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @author dulante
 * version: 1.0
 * Description:后台商品查询controller组件
 **/
@RestController
@RequestMapping(value = "/admin/goods")
public class AdminGoodsController {

    /**
     * 日志组件
     */
    private final Logger LOGGER = LoggerFactory.getLogger(AdminGoodsController.class);

    /**
     * 后台商品service组件
     */
    @Autowired
    private AdminGoodsService adminGoodsService;

    /**
     * 查询商品spu列表 （后台查询商品列表）
     *
     * @param adminGoodsSpuDTO 请求实体
     * @return 结果
     */
    @RequestMapping("/list")
    public CommonResponse list(@RequestBody AdminGoodsSpuDTO adminGoodsSpuDTO) {
        // 查询elasticsearch
        return adminGoodsService.list(adminGoodsSpuDTO);
    }

    /**
     * 根据商品spuId获取商品详情 （后台查询商品列表）
     *
     * @param id 商品spuId
     * @return 结果
     */
    @RequestMapping("/{id}")
    public CommonResponse getSpuDetailById(@PathVariable String id) {
        // 查询elasticsearch
        return adminGoodsService.getSpuDetailById(id);
    }

    /**
     * 单独添加商品spu
     *
     * @param adminGoodsSpuAddDTO 添加商品请求实体
     * @return 结果
     */
    @PostMapping("/spu/insert")
    @Deprecated
    public CommonResponse insertGoodsSpu(@RequestBody AdminGoodsSpuAddDTO adminGoodsSpuAddDTO) {
        LOGGER.info("添加商品spu，{}", JSON.toJSONString(adminGoodsSpuAddDTO));
        return adminGoodsService.insertGoodsSpu(adminGoodsSpuAddDTO.getAdminGoodsSpu(), adminGoodsSpuAddDTO.getStoreId());
    }

    /**
     * 添加商品spu和sku
     *
     * @param adminGoodsSkuAddDTO 添加商品sku请求实体
     * @return 结果
     */
    @PostMapping("/sku/insert")
    public CommonResponse insertGoodsSku(@RequestBody AdminGoodsSkuAddDTO adminGoodsSkuAddDTO) {
        LOGGER.info("添加商品spu，{}", JSON.toJSONString(adminGoodsSkuAddDTO));
        AdminGoodsSpu adminGoodsSpu = adminGoodsSkuAddDTO.getAdminGoodsSpu();
        // 如果上架时间为空，以当前时间为上架时间
        if (!StringUtils.hasLength(adminGoodsSpu.getOnlineTime())){
            adminGoodsSpu.setOnlineTime(LocalDateTime.now()
                    .format(DateTimeFormatter.ofPattern(StringPoolConstant.FULL_DATE_TIME)));
        }
        adminGoodsSpu.setParentId(adminGoodsSkuAddDTO.getStoreId());
        return adminGoodsService.insertGoodsSku(adminGoodsSpu, adminGoodsSkuAddDTO.getAdminGoodsSkuList());
    }

    /**
     * 修改商品spu
     *
     * @param adminGoodsSpu 商品spu
     * @return 结果
     */
    @PostMapping("/spu/update")
    public CommonResponse updateGoodsSpu(@RequestBody AdminGoodsSpu adminGoodsSpu) {
        LOGGER.info("修改商品spu，{}", JSON.toJSONString(adminGoodsSpu));
        return adminGoodsService.updateGoodsSpu(adminGoodsSpu);
    }

    /**
     * 修改商品sku
     *
     * @param adminGoodsSku 商品sku
     * @return 结果
     */
    @PostMapping("/sku/update")
    public CommonResponse updateGoodsSku(@RequestBody AdminGoodsSku adminGoodsSku) {
        LOGGER.info("修改商品sku，{}", JSON.toJSONString(adminGoodsSku));
        return adminGoodsService.updateGoodsSku(adminGoodsSku);
    }


    /**
     * 根据id删除商品信息
     *
     * @param id id
     * @return 结果
     */
    @GetMapping("/delete/{id}")
    public CommonResponse deleteGoodsById(@PathVariable String id) {
        return adminGoodsService.deleteGoodsById(id);
    }


    /**
     * 重建索引
     *
     * @param adminReindexDTO 请求体
     * @return 结果
     */
    @PostMapping("/reindex")
    public CommonResponse reindex(@RequestBody AdminReindexDTO adminReindexDTO){
        return adminGoodsService.reindex(adminReindexDTO.getAliasName(), adminReindexDTO.getOldIndex(),
                adminReindexDTO.getNewIndex(), adminReindexDTO.getResourceName());
    }

}