package com.ruyuan.little.project.elasticsearch.biz.api.controller;

import com.ruyuan.little.project.elasticsearch.biz.api.dto.GoodsSearchDTO;
import com.ruyuan.little.project.elasticsearch.biz.api.service.GoodsService;
import com.ruyuan.little.project.elasticsearch.biz.common.dto.CommonResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author <a href="mailto:little@163.com">little</a>
 * version: 1.0
 * Description:商品controller组件
 **/
@RestController
@RequestMapping("/api/goods")
public class GoodsController {

    /**
     * 商品服务组件
     */
    @Autowired
    private GoodsService goodsService;

    /**
     * 根据搜索字段获取自动补全结果
     *
     * @param context 搜索字段
     * @return 结果
     */
    @PostMapping("/spu/searchSuggest")
    public CommonResponse searchSpuSuggest(@RequestParam(value = "context") String context) {
//        return goodsService.searchSpuSuggestWithWildcard(context);

        // 通过suggest实现自动补全
        return goodsService.searchSpuSuggest(context);
    }


    /**
     * 分页查询商品列表
     *
     * @param goodsSearchDTO 商品查询请求信息
     * @return 结果
     */
    @PostMapping("/spu/list")
    public CommonResponse getSpuList(GoodsSearchDTO goodsSearchDTO) {
        // 指定了排序字段，查询排序的商品列表
        if (StringUtils.hasLength(goodsSearchDTO.getOrderField())) {
            return goodsService.getOrderSpuList(goodsSearchDTO);
        }
        return goodsService.getSpuList(goodsSearchDTO);
    }


    /**
     * 根据店铺id获取店铺信息
     *
     * @param id 店铺id
     * @return 结果
     */
    @RequestMapping("/store/{id}")
    public CommonResponse getStoreById(@PathVariable String id,
                                       @RequestParam(value = "page") Integer page,
                                       @RequestParam(value = "size") Integer size) {
        return goodsService.getStoreById(id, page, size);
    }

    /**
     * 根据商品spuId获取商品spu详情
     *
     * @param id id
     * @return 结果
     */
    @GetMapping("/spu/detail/{id}")
    public CommonResponse getSpuDetailById(@PathVariable String id) {
        return goodsService.getSpuDetailById(id);
    }

}
