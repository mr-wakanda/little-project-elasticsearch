package com.ruyuan.little.project.elasticsearch.biz.admin.controller;

import com.alibaba.fastjson.JSON;
import com.ruyuan.little.project.elasticsearch.biz.admin.dto.AdminGoodsStoreDTO;
import com.ruyuan.little.project.elasticsearch.biz.admin.entity.AdminGoodsStore;
import com.ruyuan.little.project.elasticsearch.biz.admin.service.AdminGoodsStoreService;
import com.ruyuan.little.project.elasticsearch.biz.common.dto.CommonResponse;
import com.ruyuan.little.project.elasticsearch.biz.common.dto.TableData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author dulante
 * version: 1.0
 * Description:后台店铺查询controller组件
 **/
@RestController
@RequestMapping(value = "/admin/goods/store")
public class AdminGoodsStoreController {

    /**
     * 日志组件
     */
    private final Logger LOGGER = LoggerFactory.getLogger(AdminGoodsStoreController.class);

    /**
     * 后台商品店铺service组件
     */
    @Autowired
    private AdminGoodsStoreService adminGoodsStoreService;

    /**
     * 根据店铺名称获取店铺列表
     *
     * @param adminGoodsStoreDTO 请求实体
     * @return 结果
     */
    @RequestMapping(value = "list", method = RequestMethod.POST)
    public CommonResponse<TableData<AdminGoodsStore>> getStorePageByStoreName(@RequestBody AdminGoodsStoreDTO adminGoodsStoreDTO) {
        // 查询MySQL
        // return goodsStoreService.getStorePageByStoreNameFromDB(storePageDTO);

        // 查询elasticsearch
        return adminGoodsStoreService.getStorePageByStoreNameFromEs(adminGoodsStoreDTO);
    }


    /**
     * 添加店铺
     *
     * @param adminGoodsStore 店铺信息
     * @return 结果
     */
    @PostMapping("insert")
    public CommonResponse insertGoodsStore(@RequestBody AdminGoodsStore adminGoodsStore) {
        LOGGER.info("添加店铺，{}", JSON.toJSONString(adminGoodsStore));
        return adminGoodsStoreService.insertGoodsStore(adminGoodsStore);
    }


    /**
     * 根据id删除商品信息
     *
     * @param id id
     * @return 结果
     */
    @GetMapping("/delete/{id}")
    public CommonResponse deleteStoreById(@PathVariable String id) {
        return adminGoodsStoreService.deleteStoreById(id);
    }

}