package com.ruyuan.little.project.elasticsearch.biz.admin.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ruyuan.little.project.elasticsearch.biz.admin.dto.AdminGoodsSpuDTO;
import com.ruyuan.little.project.elasticsearch.biz.admin.entity.AdminGoodsSku;
import com.ruyuan.little.project.elasticsearch.biz.admin.entity.AdminGoodsSpu;
import com.ruyuan.little.project.elasticsearch.biz.admin.entity.AdminGoodsSpuDetail;
import com.ruyuan.little.project.elasticsearch.biz.admin.entity.AdminGoodsStore;
import com.ruyuan.little.project.elasticsearch.biz.common.dto.CommonResponse;
import com.ruyuan.little.project.elasticsearch.biz.common.dto.TableData;
import com.ruyuan.little.project.elasticsearch.biz.common.enums.GoodsStatusEnum;
import com.ruyuan.little.project.elasticsearch.biz.admin.service.AdminGoodsService;
import com.ruyuan.little.project.elasticsearch.biz.common.constant.QueryFiledNameConstant;
import com.ruyuan.little.project.elasticsearch.biz.common.constant.StringPoolConstant;
import com.ruyuan.little.project.elasticsearch.biz.common.dao.ElasticsearchDao;
import com.ruyuan.little.project.elasticsearch.biz.common.dto.GoodsRelationField;
import com.ruyuan.little.project.elasticsearch.biz.common.utils.ElasticClientUtil;
import org.apache.commons.io.IOUtils;
import org.apache.lucene.search.join.ScoreMode;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchScrollRequest;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.UUIDs;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.IdsQueryBuilder;
import org.elasticsearch.index.query.InnerHitBuilder;
import org.elasticsearch.index.query.MatchAllQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.RangeQueryBuilder;
import org.elasticsearch.index.query.TermQueryBuilder;
import org.elasticsearch.join.query.HasChildQueryBuilder;
import org.elasticsearch.join.query.HasParentQueryBuilder;
import org.elasticsearch.search.Scroll;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.PathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author dulante
 * version: 1.0
 * Description:后台商品service实现类组件
 **/
@Service
public class AdminGoodsServiceImpl implements AdminGoodsService {

    /**
     * 日志组件
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(AdminGoodsServiceImpl.class);

    /**
     * es rest client api
     */
    @Autowired
    private RestHighLevelClient restHighLevelClient;

    /**
     * elasticsearch Dao 组件
     */
    @Autowired
    private ElasticsearchDao elasticsearchDao;

    /**
     * 店铺index别名
     */
    @Value("${elasticsearch.goodsIndexAlias}")
    private String goodsIndexAlias;

    /**
     * 店铺index名称
     */
    @Value("${elasticsearch.goodsIndex}")
    private String goodsIndex;

    /**
     * 自动上架分页数据大小
     */
    @Value("${elasticsearch.autoOnlinePageSize}")
    private Integer autoOnlinePageSize;

    /**
     * scroll查询失效时间，单位：秒
     */
    @Value("${elasticsearch.scrollExpireSeconds}")
    private Long scrollExpireSeconds;

    /**
     * 店铺的mapping
     */
    @Value(value = "classpath:/mapping/goodsIndexSecondVersion.json")
    private Resource goodsIndexMappingResource;

    /**
     * mapping资源路径
     */
    @Value("${elasticsearch.resourcePath}")
    private String resourcePath;

    /**
     * 商品文档json
     */
    @Value(value = "classpath:/mapping/goodsDocFirstVersion.json")
    private Resource goodsDocResource;

    /**
     * 第一个商品spuId，根据商品初始化文档固定一个数据
     */
    private String firstGoodsSpuId = "4";

    /**
     * 初始化店铺的索引
     *
     * @throws IOException
     */
    @PostConstruct
    public void init() throws IOException {
        // 查询商品索引是否存在
        boolean storeIndexExistFlag = elasticsearchDao.indexExistFlag(goodsIndex);
        if (!storeIndexExistFlag) {
            // 不存在
            // 创建商品索引
            Boolean created = elasticsearchDao.createIndex(goodsIndex, goodsIndexAlias, goodsIndexMappingResource);
            LOGGER.info("create index {} ", created ? "success" : "fail");
        } else {
            LOGGER.info("index:{} already exist", goodsIndex);
        }
        // 查询商品信息是否存在，根据初始化数据来指定对应的id
        boolean documentExistFlag = elasticsearchDao.docExistFlag(goodsIndex,firstGoodsSpuId);
        if (!documentExistFlag) {
            // 不存在
            // 初始化商品信息
            this.initGoodsInfo();
            LOGGER.info("initialization goods info finished");
        } else {
            LOGGER.info("goods info already exist");
        }
    }

    /**
     * 初始化商品信息
     * @throws IOException
     */
    private void initGoodsInfo() throws IOException {
        String docJson = IOUtils.toString(goodsDocResource.getInputStream());
        JSONObject docJsonObject = JSON.parseObject(docJson);
        BulkRequest bulkRequest = new BulkRequest();
        // 店铺信息
        JSONArray storeJsonArray = docJsonObject.getJSONArray(GoodsRelationField.GOODS_STORE);
        LOGGER.info("初始化店铺信息：{}",storeJsonArray.toJSONString());
        for (int i = 0; i < storeJsonArray.size(); i++) {
            JSONObject store = storeJsonArray.getJSONObject(i);
            IndexRequest indexRequest = new IndexRequest(goodsIndexAlias);
            indexRequest.id(store.getString(StringPoolConstant.ID));
            indexRequest.source(store.toJSONString(), XContentType.JSON);
//            elasticsearchDao.insert(indexRequest);
            bulkRequest.add(indexRequest);
        }

        // 商品spu信息
        JSONArray goodsSpuJsonArray = docJsonObject.getJSONArray(GoodsRelationField.GOODS_SPU);
        if (CollectionUtils.isEmpty(goodsSpuJsonArray)){
            return;
        }
        LOGGER.info("初始化商品spu信息：{}",goodsSpuJsonArray.toJSONString());
        for (int i = 0; i < goodsSpuJsonArray.size(); i++) {
            JSONObject goodsSpu = goodsSpuJsonArray.getJSONObject(i);
            IndexRequest indexRequest = new IndexRequest(goodsIndexAlias);
            indexRequest.id(goodsSpu.getString(StringPoolConstant.ID));
            indexRequest.routing(goodsSpu.getString(StringPoolConstant.ROUTING));
            // 移除多余字段
            goodsSpu.remove(StringPoolConstant.ROUTING);
            indexRequest.source(goodsSpu.toJSONString(), XContentType.JSON);
//            elasticsearchDao.insert(indexRequest);
            bulkRequest.add(indexRequest);
        }

        // 商品sku信息
        JSONArray goodsSkuJsonArray = docJsonObject.getJSONArray(GoodsRelationField.GOODS_SKU);
        LOGGER.info("初始化商品sku信息：{}",goodsSkuJsonArray.toJSONString());
        for (int i = 0; i < goodsSkuJsonArray.size(); i++) {
            JSONObject goodsSku = goodsSkuJsonArray.getJSONObject(i);
            IndexRequest indexRequest = new IndexRequest(goodsIndexAlias);
            indexRequest.id(goodsSku.getString(StringPoolConstant.ID));
            indexRequest.routing(goodsSku.getString(StringPoolConstant.ROUTING));
            // 移除多余字段
            goodsSku.remove(StringPoolConstant.ROUTING);
            indexRequest.source(goodsSku.toJSONString(), XContentType.JSON);
//            elasticsearchDao.insert(indexRequest);
            bulkRequest.add(indexRequest);
        }
        // 批量初始化sku信息
        elasticsearchDao.bulk(bulkRequest);
    }

    @Override
    public CommonResponse list(AdminGoodsSpuDTO adminGoodsSpuDTO) {
        LOGGER.info("开始调用AdminGoodsServiceImpl类list方法, 方法使用参数:[[adminGoodsSpuDTO]:{}]", JSON.toJSONString(adminGoodsSpuDTO));

        // 请求参数
        String goodsStoreId = adminGoodsSpuDTO.getStoreId();
        Integer page = adminGoodsSpuDTO.getPage();
        Integer size = adminGoodsSpuDTO.getSize();

        // 构建商品分页查询请求体
        SearchSourceBuilder sourceBuilder = this.buildGoodsPageQueryBuilder(goodsStoreId, adminGoodsSpuDTO.getGoodsName(), page, size);

        // 查询请求
        SearchRequest searchRequest = new SearchRequest(goodsIndexAlias);
        searchRequest.source(sourceBuilder);

        try {
            LOGGER.info("从es查询商品请求参数：{}", searchRequest.source().toString());
            SearchResponse searchResponse = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);

            // 构建查询结果
            TableData<AdminGoodsSpu> tableData = this.buildGoodsPageResult(searchResponse);
            LOGGER.info("结束调用AdminGoodsServiceImpl类list方法,结果:{}", JSON.toJSONString(tableData));
            return CommonResponse.success(tableData);
        } catch (IOException e) {
            LOGGER.error("从es查询店铺列表失败", e);
        }
        return CommonResponse.fail();
    }

    /**
     * 构建商品分页结果
     *
     * @param searchResponse es查询响应结果
     * @return 店铺列表
     */
    private TableData<AdminGoodsSpu> buildGoodsPageResult(SearchResponse searchResponse) {
        TableData<AdminGoodsSpu> tableData = new TableData<>();
        SearchHits hits = searchResponse.getHits();
        long total = hits.getTotalHits().value;
        tableData.setTotal(total);

        // 店铺列表
        List<AdminGoodsSpu> adminGoodsSpuList = new ArrayList<>();
        if (total > 0) {
            // 封装数据
            for (SearchHit hit : hits) {
                AdminGoodsSpu adminGoodsSpu = JSON.parseObject(hit.getSourceAsString(), AdminGoodsSpu.class);
                adminGoodsSpuList.add(adminGoodsSpu);
            }
        }
        tableData.setRows(adminGoodsSpuList);
        return tableData;
    }

    /**
     * 构建分页查询商品信息的请求体
     *
     * @param goodsStoreId 店铺id
     * @param goodsName    商品名称
     * @param page         页码
     * @param size         每页大小
     * @return 查询资源构建器
     */
    private SearchSourceBuilder buildGoodsPageQueryBuilder(String goodsStoreId, String goodsName, Integer page, Integer size) {
        // 获取分页查询构造器
        SearchSourceBuilder sourceBuilder = ElasticClientUtil.builderPageSearchBuilder(page, size);

        // 指定父文档的筛选条件
        QueryBuilder parentQueryBuilder;

        // 商品店铺id不为空，根据id筛选，否则查询所有
        if (StringUtils.hasLength(goodsStoreId)) {
            parentQueryBuilder = new TermQueryBuilder(QueryFiledNameConstant.UNDERSCORE_ID, goodsStoreId);
        }else {
            parentQueryBuilder = new MatchAllQueryBuilder();
        }

        // 指定hasParent查询条件，限定查询结果的父文档类型是 店铺：goodsStore
        HasParentQueryBuilder hasParentQueryBuilder =
                new HasParentQueryBuilder(GoodsRelationField.GOODS_STORE, parentQueryBuilder, false)
                        .innerHit(new InnerHitBuilder());
        BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery().must(hasParentQueryBuilder);


        // 商品名称是否为空
        if (StringUtils.hasLength(goodsName)) {
            boolQueryBuilder.must(QueryBuilders.wildcardQuery(QueryFiledNameConstant.GOODS_NAME,
                                                              StringPoolConstant.STAR + goodsName + StringPoolConstant.STAR));
        }

        sourceBuilder.query(boolQueryBuilder);

        return sourceBuilder;
    }


    @Override
    public CommonResponse getSpuDetailById(String id) {
        // 构建根据id查询商品spu详情的查询资源构建器
        SearchSourceBuilder sourceBuilder = this.buildSpuDetailByIdSource(id);

        // 查询请求
        SearchRequest searchRequest = new SearchRequest(goodsIndexAlias);
        searchRequest.source(sourceBuilder);
        try {
            LOGGER.info("ES请求参数：{}", searchRequest.source().toString());
            SearchResponse searchResponse = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);

            // 构建查询结果
            AdminGoodsSpuDetail adminGoodsSpuDetail = this.buildSpuDetailByIdResult(id, searchResponse);
            LOGGER.info("结束调用AdminGoodsServiceImpl类getSpuDetailById方法,结果:{}", JSON.toJSONString(adminGoodsSpuDetail));

            return CommonResponse.success(adminGoodsSpuDetail);
        } catch (IOException e) {
            LOGGER.error("查询商品明细失败", e);
        }
        return CommonResponse.fail();
    }

    /**
     * 构建根据id查询商品spu详情的查询资源构建器
     *
     * @param id 商品spuId
     * @return 资源构建器
     */
    private SearchSourceBuilder buildSpuDetailByIdSource(String id) {
        // 根据id查询商品spu
        IdsQueryBuilder idsQueryBuilder = QueryBuilders.idsQuery().addIds(id);

        // 查询商品spu的父类型，店铺信息
        HasParentQueryBuilder hasParentQueryBuilder =
                new HasParentQueryBuilder(GoodsRelationField.GOODS_STORE, QueryBuilders.matchAllQuery(), false)
                        .innerHit(new InnerHitBuilder());

        // 查询商品spu的子类型，商品sku信息，InnerHitBuilder默认size是3，因为商品spu下面的商品sku很可能不止3，所以设置一下size。
        HasChildQueryBuilder hasChildQueryBuilder =
                new HasChildQueryBuilder(GoodsRelationField.GOODS_SKU, QueryBuilders.matchAllQuery(), ScoreMode.None)
                        .innerHit(new InnerHitBuilder().setSize(GoodsRelationField.GOODS_SKU_MAX_SIZE));

        // bool查询构建器，整合查询条件
        BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder();
        boolQueryBuilder.must(idsQueryBuilder).must(hasParentQueryBuilder).must(hasChildQueryBuilder);

        // 查询资源构建器
        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
        sourceBuilder.query(boolQueryBuilder);
        return sourceBuilder;
    }

    /**
     * 构建根据id查询商品spu的查询结果
     *
     * @param id             商品id
     * @param searchResponse 查询结果
     * @return 结果
     */
    private AdminGoodsSpuDetail buildSpuDetailByIdResult(String id, SearchResponse searchResponse) {
        // 商品spu信息
        SearchHits hits = searchResponse.getHits();
        if (hits.getTotalHits().value <= 0) {
            // 商品不存在
            throw new RuntimeException("商品id:" + id + "不存在");
        }

        // 商品存在 封装响应信息
        SearchHit searchHit = hits.getHits()[0];
        // 商品详情
        AdminGoodsSpuDetail adminGoodsSpuDetail = JSON.parseObject(searchHit.getSourceAsString(), AdminGoodsSpuDetail.class);

        // 封装店铺名称
        this.builderStoreName(searchHit, adminGoodsSpuDetail);

        // 商品sku列表信息
        this.builderSkuList(searchHit, adminGoodsSpuDetail);

        return adminGoodsSpuDetail;
    }

    /**
     * 构建商品sku列表信息
     *
     * @param searchHit             查询结果
     * @param adminGoodsSpuDetail   商品spu详细信息
     */
    private void builderSkuList(SearchHit searchHit, AdminGoodsSpuDetail adminGoodsSpuDetail) {

        // hasParent或者hasChild查询结果
        Map<String, SearchHits> innerHits = searchHit.getInnerHits();

        SearchHits skuSearchHits = innerHits.get(GoodsRelationField.GOODS_SKU);

        // 封装商品sku数据
        List<AdminGoodsSku> adminGoodsSkuList = new ArrayList<>();
        for (SearchHit hit : skuSearchHits) {
            AdminGoodsSku adminGoodsSku = JSON.parseObject(hit.getSourceAsString(), AdminGoodsSku.class);
            adminGoodsSkuList.add(adminGoodsSku);
        }
        adminGoodsSpuDetail.setAdminGoodsSkuList(adminGoodsSkuList);
    }

    /**
     * 封装店铺名称
     *
     * @param searchHit             查询结果
     * @param adminGoodsSpuDetail   商品spu详细信息
     */
    private void builderStoreName(SearchHit searchHit, AdminGoodsSpuDetail adminGoodsSpuDetail) {
        // hasParent或者hasChild查询结果
        Map<String, SearchHits> innerHits = searchHit.getInnerHits();
        // 店铺信息
        SearchHits storeSearchHits = innerHits.get(GoodsRelationField.GOODS_STORE);
        AdminGoodsStore adminGoodsStore = JSON.parseObject(storeSearchHits.getHits()[0].getSourceAsString(), AdminGoodsStore.class);
        adminGoodsSpuDetail.setStoreName(adminGoodsStore.getStoreName());
    }

    @Override
    public CommonResponse insertGoodsSpu(AdminGoodsSpu adminGoodsSpu, String parentId) {
        adminGoodsSpu.setId(UUIDs.base64UUID());
        IndexRequest indexRequest = new IndexRequest(goodsIndexAlias);
        indexRequest.id(adminGoodsSpu.getId());
        adminGoodsSpu.setGoodsSpuNo(UUIDs.base64UUID());
        adminGoodsSpu.setParentId(parentId);
        // 指定路由
        indexRequest.routing(parentId);
        indexRequest.source(JSONObject.toJSONString(adminGoodsSpu), XContentType.JSON);
        return elasticsearchDao.insert(indexRequest);
    }

    @Override
    public CommonResponse insertGoodsSku(AdminGoodsSpu adminGoodsSpu, List<AdminGoodsSku> adminGoodsSkuList) {
        if (ObjectUtils.isEmpty(adminGoodsSpu.getId())) {
            CommonResponse response = insertGoodsSpu(adminGoodsSpu, adminGoodsSpu.getGoodsRelationField().getParent());
            adminGoodsSpu.setId(String.valueOf(response.getData()));
        }
        // 单条商品sku循环insert到es中
//        adminGoodsSkuList.forEach(adminGoodsSku -> {
//            adminGoodsSku.setParentId(adminGoodsSpu.getId());
//            adminGoodsSku.setId(UUIDs.base64UUID());
//            IndexRequest indexRequest = new IndexRequest(goodsIndexAlias);
//            indexRequest.id(adminGoodsSku.getId());
//            indexRequest.routing(adminGoodsSpu.getGoodsRelationField().getParent());
//            indexRequest.source(JSONObject.toJSONString(adminGoodsSku), XContentType.JSON);
//            elasticsearchDao.insert(indexRequest);
//        });
//        return CommonResponse.success();

//        使用bulk一次性insert多条商品sku到es中
        BulkRequest bulkRequest = new BulkRequest();
        adminGoodsSkuList.forEach(adminGoodsSku -> {
            adminGoodsSku.setParentId(adminGoodsSpu.getId());
            adminGoodsSku.setId(UUIDs.base64UUID());
            IndexRequest indexRequest = new IndexRequest(goodsIndexAlias);
            indexRequest.id(adminGoodsSku.getId());
            indexRequest.routing(adminGoodsSpu.getGoodsRelationField().getParent());
            indexRequest.source(JSONObject.toJSONString(adminGoodsSku), XContentType.JSON);
            bulkRequest.add(indexRequest);
        });
        return elasticsearchDao.bulk(bulkRequest);
    }

    @Override
    public CommonResponse updateGoodsSpu(AdminGoodsSpu adminGoodsSpu) {
        UpdateRequest updateRequest = new UpdateRequest(goodsIndexAlias, adminGoodsSpu.getId());
        updateRequest.doc(JSONObject.toJSONString(adminGoodsSpu), XContentType.JSON);
        return elasticsearchDao.update(updateRequest);
    }

    @Override
    public CommonResponse updateGoodsSku(AdminGoodsSku adminGoodsSku) {
        UpdateRequest updateRequest = new UpdateRequest(goodsIndexAlias, adminGoodsSku.getId());
        updateRequest.doc(JSONObject.toJSONString(adminGoodsSku), XContentType.JSON);
        return elasticsearchDao.update(updateRequest);
    }


    @Override
    public CommonResponse deleteGoodsById(String id) {
        DeleteRequest deleteRequest = new DeleteRequest(goodsIndexAlias, id);
        return elasticsearchDao.delete(deleteRequest);
    }

    @Override
    public void autoOnlineGoodsSpuByOnlineTime(String onlineTime) {
        // 构建查询指定时间之前未上架的商品信息的请求体
        SearchSourceBuilder sourceBuilder = this.buildOnlineTimeGoodsSpuSource(onlineTime);

        // 查询请求
        SearchRequest searchRequest = new SearchRequest(goodsIndexAlias);
        searchRequest.source(sourceBuilder);

        try {
            LOGGER.info("ES请求参数：{}", searchRequest.source().toString());
            SearchResponse searchResponse = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);

            // 构建查询结果
            List<AdminGoodsSpu> adminGoodsSpuList = this.buildAutoOnlineSearchResult(searchResponse);
            // 自动上架商品
            this.autoOnlineGoodsSpu(adminGoodsSpuList);

            LOGGER.info("结束调用AdminGoodsServiceImpl类autoOnlineGoodsSpuWithBeforeTime方法");
        } catch (IOException e) {
            LOGGER.error("自动上架指定时间之前的商品", e);
        }
    }

    @Override
    public void autoOnlineGoodsSpuByOnlineTimeScroll(String onlineTime) {
        // 构建查询指定时间之前未上架的商品信息的请求体
        SearchSourceBuilder sourceBuilder = this.buildOnlineTimeGoodsSpuSource(onlineTime);

        // 查询请求
        SearchRequest searchRequest = new SearchRequest(goodsIndexAlias);
        // 指定scroll失效时间为60秒，
        Scroll scroll = new Scroll(TimeValue.timeValueSeconds(scrollExpireSeconds));
        searchRequest.scroll(scroll);
        // 设定每次查询数据量大小
        sourceBuilder.size(autoOnlinePageSize);
        searchRequest.source(sourceBuilder);

        try {
            LOGGER.info("ES请求参数：{}", searchRequest.source().toString());
            SearchResponse searchResponse = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);

            // scroll查询的id
            String scrollId = searchResponse.getScrollId();

            // 构建查询结果
            List<AdminGoodsSpu> adminGoodsSpuList = this.buildAutoOnlineSearchResult(searchResponse);

            while (!CollectionUtils.isEmpty(adminGoodsSpuList)) {
                // 自动上架商品
                autoOnlineGoodsSpu(adminGoodsSpuList);

                // 重新构建scroll查询
                SearchScrollRequest scrollRequest = new SearchScrollRequest(scrollId);
                scrollRequest.scroll(scroll);
                // 发起查询
                searchResponse = restHighLevelClient.scroll(scrollRequest, RequestOptions.DEFAULT);
                // scroll查询的id
                scrollId = searchResponse.getScrollId();
                // 构建查询结果
                adminGoodsSpuList = this.buildAutoOnlineSearchResult(searchResponse);
            }
            LOGGER.info("结束调用AdminGoodsServiceImpl类autoOnlineGoodsSpuWithBeforeTimeWithScroll方法");
        } catch (IOException e) {
            LOGGER.error("自动上架指定时间之前的商品", e);
        }
    }

    /**
     * 构建自动上架商品查询结果集
     *
     * @param searchResponse es查询响应
     * @return 结果
     */
    private List<AdminGoodsSpu> buildAutoOnlineSearchResult(SearchResponse searchResponse) {
        List<AdminGoodsSpu> adminGoodsSpuList = new ArrayList<>();
        for (SearchHit hit : searchResponse.getHits()) {
            AdminGoodsSpu adminGoodsSpu = JSON.parseObject(hit.getSourceAsString(), AdminGoodsSpu.class);
            adminGoodsSpuList.add(adminGoodsSpu);
        }
        return adminGoodsSpuList;
    }

    /**
     * 自动上架商品
     *
     * @param adminGoodsSpuList 商品列表
     */
    private void autoOnlineGoodsSpu(List<AdminGoodsSpu> adminGoodsSpuList) {
        // 单条商品数据循环update
//        adminGoodsSpuList.forEach(adminGoodsSpu -> {
//            adminGoodsSpu.setGoodsStatus(GoodsStatusEnum.ONLINE.getCode());
//            UpdateRequest updateRequest = new UpdateRequest(goodsIndexAlias, adminGoodsSpu.getId());
//            updateRequest.doc(JSONObject.toJSONString(adminGoodsSpu), XContentType.JSON);
//            elasticsearchDao.update(updateRequest);
//        });

        // 使用bulk多条商品数据一次性update
        BulkRequest bulkRequest = new BulkRequest();
        adminGoodsSpuList.forEach(adminGoodsSpu -> {
            adminGoodsSpu.setGoodsStatus(GoodsStatusEnum.ONLINE.getCode());
            UpdateRequest updateRequest = new UpdateRequest(goodsIndexAlias, adminGoodsSpu.getId());
            updateRequest.doc(JSONObject.toJSONString(adminGoodsSpu), XContentType.JSON);
            bulkRequest.add(updateRequest);
        });
        elasticsearchDao.bulk(bulkRequest);

    }

    /**
     * 构建查询指定时间之前未上架的商品信息的请求体
     *
     * @param dateTime 指定时间
     * @return 请求体
     */
    private SearchSourceBuilder buildOnlineTimeGoodsSpuSource(String dateTime) {
        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
        // 构建查询所有未上架的上架时间在指定时间之前的商品
        TermQueryBuilder statusQuery =
                QueryBuilders.termQuery(QueryFiledNameConstant.GOODS_STATUS, GoodsStatusEnum.OFFLINE.getCode());
        RangeQueryBuilder timeQuery = QueryBuilders.rangeQuery(QueryFiledNameConstant.ONLINE_TIME).lte(dateTime);
        sourceBuilder.query(new BoolQueryBuilder().filter(timeQuery).filter(statusQuery));
        return sourceBuilder;
    }


    @Override
    public CommonResponse reindex(String indexAlias, String oldIndex, String newIndex, String resourceName){
        try {
            Resource mappingResource = new PathResource(resourcePath + resourceName);
            // 创建新的商品索引 newIndex
            Boolean created = elasticsearchDao.createIndex(newIndex, indexAlias, mappingResource);
            if (created){
                // 将oldIndex的数据，写入newIndex中
                elasticsearchDao.reindex(oldIndex, newIndex);

                // 将索引别名指向的索引从oldIndex修改为newIndex
                Boolean changed = elasticsearchDao.changeAliasAfterReindex(indexAlias,oldIndex, newIndex);
                if (changed){
                    return CommonResponse.success();
                }
            }
        } catch (IOException e) {
            LOGGER.error("reindex fail, oldIndex :{} newIndex :{} alias :{}", oldIndex, newIndex, indexAlias, e);
        }
        return CommonResponse.fail();
    }
}